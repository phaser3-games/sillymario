/**
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene1 extends Phaser.Scene {
    constructor() {
        super();
        this.player = null;
        this.platforms = null;
        this.cursors = null;
    }
    preload() {
        this.load.image('tiles', 'assets/nature-paltformer-tileset-16x16.png');
        this.load.image('mushroom', 'assets/mushroom.png');
        this.load.tilemapTiledJSON('map', 'assets/level1.json');
        this.load.spritesheet('Mario',
            'assets/Mario.png',
            { frameWidth: 32, frameHeight: 18 }
        );
        this.load.spritesheet('Enemy',
            'assets/Enemies.png',
            { frameWidth: 32, frameHeight: 19 }
        );
        this.load.spritesheet('SuperMario',
            'assets/SuperMario.png',
            { frameWidth: 32, frameHeight: 18 }
        );
        this.load.image('coin', 'assets/tile054.png');
    }
    create() {
        const map = this.make.tilemap({ key: 'map' });
        this.cameras.main.setBounds(0, 0, 3200, 640);
        const tileset = map.addTilesetImage('natural', 'tiles');
        this.platforms = map.createStaticLayer('MapLayer', tileset, 0, 0);
        map.setCollisionByExclusion([22,75, 37]);

        this.coins = map.createFromObjects('CoinLayer', 55, { key: 'coin' });
        this.coins.forEach(coin => {
            this.physics.world.enable(coin);
            coin.body.setImmovable(true);
            coin.body.setAllowGravity(false);
        });

        this.mushroom=new Mushroom(this, 800, 300);
        this.anims.create({
            key: 'marioturn',
            frames: [{ key: 'Mario', frame: 0 }],
            frameRate: 20
        });

        this.anims.create({
            key: 'marioright',
            frames: this.anims.generateFrameNumbers('Mario', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'enemymove',
            frames: this.anims.generateFrameNumbers('Enemy', { start: 0, end: 2 }),
            frameRate: 2,
            repeat: -1
        });

        this.anims.create({
            key: 'supermarioturn',
            frames: [{ key: 'SuperMario', frame: 0 }],
            frameRate: 20
        });

        this.anims.create({
            key: 'supermarioright',
            frames: this.anims.generateFrameNumbers('SuperMario', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        let self=this;
        this.player = new Player(this, 100, 450);
        this.ememies=new Array();
        for(let i=0; i<10; i++){
            let enemy=new Enemy(this, 3200*Math.random(), 640*Math.random());
            this.ememies.push(enemy);
        }

        this.cameras.main.startFollow(this.player);
        this.physics.add.collider(this.player, this.platforms);
        this.physics.add.collider(this.ememies, this.platforms);
        this.physics.add.collider(this.ememies, this.player, function(e, p){
            if(p.body.touching.down && e.body.touching.up){
                p.setVelocityY(-30);
                e.disableBody(true, true);
            }else{
                if(p.superMode){
                    p.superMode=false;
                    e.disableBody(true, true);
                }else{
                    p.disableBody(true, true);
                }
            }
        });
        this.physics.add.collider(this.mushroom, this.platforms);
        this.physics.add.overlap(this.mushroom, this.player, function(m, p){
            m.disableBody(true, true);
            p.superMode=true;
        });
        this.cursors = this.input.keyboard.createCursorKeys();
        this.physics.add.collider(this.platforms, this.coins);
        this.physics.add.overlap(this.player, this.coins, function(p, c){
            c.destroy();
        });
        this.physics.add.overlap(this.player, this.platforms, function(p, t){
            if(t.index==22){
                if(p.body.blocked.down){
                    p.y=p.y-3;
                }
                p.onLadder=true;
                p.body.setAllowGravity(false);
            }else if(p.tryOffLadder){
                p.tryOffLadder=false;
                p.onLadder=false;
                p.body.setAllowGravity(true);
            }
        });
    }

    update() {
        if (this.cursors.left.isDown) {
            this.player.flipX=true;
            this.player.setVelocityX(this.player.superMode?-250:-160);
            this.player.anims.play(this.player.superMode?'supermarioright':'marioright', true);
            if(this.player.onLadder){
                this.player.tryOffLadder=true;
            }
        }
        else if (this.cursors.right.isDown) {
            this.player.flipX=false;
            this.player.setVelocityX(this.player.superMode?250:160);
            this.player.anims.play(this.player.superMode?'supermarioright':'marioright', true);
            if(this.player.onLadder){
                this.player.tryOffLadder=true;
            }
        }
        else {
            this.player.setVelocityX(0);
            this.player.anims.play(this.player.superMode?'supermarioturn':'marioturn', true);
        }
        if (this.cursors.up.isDown) {
            if(this.player.onLadder){
                this.player.y=this.player.y-1;
            }else if(this.player.body.blocked.down){
                this.player.setVelocityY(this.player.superMode?-300:-200);
            }
        }
        if (this.cursors.down.isDown) {
            if(this.player.onLadder){
                this.player.y=this.player.y+1;
            }
        }
        this.mushroom.update();
        for(let e of this.ememies){
            e.anims.play('enemymove', true);
            e.update();
        }
    }
}