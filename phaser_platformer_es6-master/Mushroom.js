class Mushroom extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y){
        super(scene, x, y, "mushroom");
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.setBounce(0.5);
        this.direction=-1;
        let self=this;
        setInterval(function(){
            self.direction=self.direction*(-1);
        }, 10000);
    }

    update(){
        this.setVelocityX(this.direction*30);
    }
}